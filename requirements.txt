# This file may be used to create an environment using:
# $ conda create --name storywrangler --file <this file>
fasttext==0.9.2
dask
dill
joblib
matplotlib
numpy
pandas
plotly
pymongo
scikit-learn
scipy
seaborn
statsmodels
tqdm
ujson